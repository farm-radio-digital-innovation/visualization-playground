import React, { useState, useEffect, useContext } from "react";
import MapChart from './MapChart';
import Scene from './Scene';
import ReactTooltip from 'react-tooltip';
import { SceneContext, SceneContextProvider } from './contexts/Scene';
import toast, { Toaster } from 'react-hot-toast';

function scheduleToast(delay, previousMsgNo) {
  let msgNo;
  while ((msgNo = Math.floor(Math.random()*testMsgs.length)) === previousMsgNo)
    ;
  setTimeout(() => {
    toast(testMsgs[msgNo], { 
      duration: 6000,
      style: {
        background: '#202020D0',
        padding: '12px',
        color: '#fff',
      }
    });
    scheduleToast(700+7000*Math.random(), msgNo);
  }, delay);
}

function SceneWrapper() {
  const [content, setContent] = useState("");
  const {setMousePosition, mousePosition} = useContext(SceneContext);

  const handleMouseMove = ({ pageX, pageY }) => {
    setMousePosition({
      left: pageX,
      top: pageY,
    });
  };

  return (
    <div>
      <Scene />
      <div style={{ width: '100%', height: '500px', overflow: 'hidden' }}>
        <MapChart setTooltipContent={setContent} />
        <ReactTooltip>{content}</ReactTooltip>
      </div>
    </div>
  );
}

function App() {
  useEffect(() => {
    scheduleToast(2000);
  }, []);

  return (
    <div style={{ height: '100vh', display: 'flex', flexDirection: 'column' }}>
      <Toaster 
        position="top-left"
        reverseOrder={true}
      />
      <SceneContextProvider>
        <SceneWrapper />
      </SceneContextProvider>
    </div>
  );
}

const testMsgs = [
  "Hi! I am calling from Reykjavík. I'd like to get into aqua farming!",
  "Any fish farmers out there?",
  "I have a qustion: Is it possible to grow spirulina in my bath tub?",
  "Keep up the good work.",
  "👏 Just saying 'Hi!'",
  "I need more information on fertilizers",
  "What is rational is actual and what is actual is rational",
  "One cannot step twice in the same river.",
  "One cannot conceive anything so strange and so implausible that it has not already been said by one philosopher or another",
  "Life must be understood backward. But it must be lived forward.",
  "Science is what you know. Philosophy is what you don't know.",
  "Happiness is the highest good",
  "Once upon a midnight dreary, while I pondered, weak and weary...",
  "If you would be a real seeker after truth, it is necessary that at least once in your life you doubt, as far as possible, all things",
  "Philosophers have hitherto only interpreted the world in various ways; the point, however, is to change it",
  "Everything that exists is born for no reason, carries on living through weakness, and dies by accident",
  "It is too hot for farming over here.",
  "Is mosquito farming worthwhile?"
];

export default App;
