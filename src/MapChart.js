import React, { memo } from "react";
import {
  Annotation,
  ZoomableGroup,
  ComposableMap,
  Geographies,
  Geography
} from "react-simple-maps";

//const geoUrl =
//  "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

const geoUrl =
  "https://raw.githubusercontent.com/deldersveld/topojson/master/continents/africa.json";

const rounded = num => {
  if (num > 1000000000) {
    return Math.round(num / 100000000) / 10 + "Bn";
  } else if (num > 1000000) {
    return Math.round(num / 100000) / 10 + "M";
  } else {
    return Math.round(num / 100) / 10 + "K";
  }
};

const MapChart = ({ setTooltipContent }) => {
  return (
    <>
      <ComposableMap style={{ height: '100%', width: '100%' }} data-tip="" projectionConfig={{ scale: 300 }}>
        <ZoomableGroup>
          <Geographies geography={geoUrl}>
            {({ geographies }) =>
              geographies.map(geo => (
                <Geography
                  key={geo.rsmKey}
                  geography={geo}
                  onMouseEnter={() => {
                    const { NAME, POP_EST } = geo.properties;
                    setTooltipContent(`${NAME} — ${rounded(POP_EST)}`);
                  }}
                  onMouseLeave={() => {
                    setTooltipContent("");
                  }}
                  onClick={() => {
                  }}
                  style={{
                    default: {
                      fill: "#b97c51",
                      outline: "none"
                    },
                    hover: {
                      fill: "#813200",
                      outline: "none"
                    },
                    pressed: {
                      fill: "#E42",
                      outline: "none"
                    }
                  }}
                />
              ))
            }
          </Geographies>
          <Annotation
            subject={[32.6201, 0.3159]}
            dx={0}
            dy={0}
            connectorProps={{
              stroke: "#413200",
              strokeWidth: 7,
              strokeLinecap: "round"
            }}
          >
            <text x="-9" textAnchor="end" alignmentBaseline="middle" fill="#413200">
              {"Kampala"}
            </text>
          </Annotation>
          <Annotation
            subject={[0.1870, 5.6037]}
            dx={0}
            dy={0}
            connectorProps={{
              stroke: "#413200",
              strokeWidth: 7,
              strokeLinecap: "round"
            }}
          >
            <text x="-9" textAnchor="end" alignmentBaseline="middle" fill="#413200">
              {"Accra"}
            </text>
          </Annotation>
          <Annotation
            subject={[39.2083, -6.7924]}
            dx={0}
            dy={0}
            connectorProps={{
              stroke: "#413200",
              strokeWidth: 7,
              strokeLinecap: "round"
            }}
          >
            <text x="-9" textAnchor="end" alignmentBaseline="middle" fill="#413200">
              {"Dar es Salaam"}
            </text>
          </Annotation>
        </ZoomableGroup>
      </ComposableMap>
    </>
  );
};

export default memo(MapChart);
