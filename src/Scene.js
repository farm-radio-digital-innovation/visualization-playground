import React, { useState, useEffect, useContext } from "react";
import { Canvas, useFrame } from '@react-three/fiber'
import { SceneContext } from './contexts/Scene';

function Pieces({ items, toggleSelected, rotation, setRotation }) {

  useFrame(({ clock }) => {
    const rnd = (f) => Math.sin(clock.elapsedTime * f) * 0.2;
    setRotation([rnd(0.8), rnd(0.3), rnd(0.2)]);
  });

  const handleClick = ((i) => {
    toggleSelected(i);
  });

  return (
    <object3D rotation={rotation}>
      {items.map(({ position, shape, rotation, selected }, i) => 
        <mesh 
          scale={selected ? 1.6 : 1}
          onClick={() => handleClick(i)}
          key={i} 
          position={position} 
          rotation={rotation}
        >
          {shape === 0 ? (
            <sphereGeometry args={[30, 100, 90, 3]} />
          ) : shape === 1 ? (
            <boxGeometry args={[50, 50, 40, 3]} />
          ) : shape === 2 ? (
            <coneGeometry args={[40, 80, 30, 5]} />
          ) : (
            <>
            </>
          )}
          {selected ? (
            <meshStandardMaterial color="cyan" />
          ) : (
            <meshStandardMaterial color="hotpink" />
          )}
        </mesh>
      )}
    </object3D>
  );
}

export default function Scene() {
  const [cameraPostion, setCameraPosition] = useState([0, 0, 0]);

  const { mousePosition, rotation, setRotation, items, toggleSelected } = useContext(SceneContext);

//      camera.position.x += ( mouseX - camera.position.x ) * .0080;
//                camera.position.y += ( - mouseY - camera.position.y ) * .0080;
//
//                camera.lookAt( scene.position );

  return (
    <div style={{ height: '100vh', flexDirection: 'column' }}>
      <Canvas camera={{ fov: 50, near: 0.1, far: 1300, position: cameraPostion }} style={{ display: 'flex', height: '100vh' }}>
        <ambientLight intensity={0.5} />
        <pointLight position={[10, 0, 10]} />
        <Pieces 
          items={items} 
          toggleSelected={toggleSelected} 
          rotation={rotation} 
          setRotation={setRotation} 
        />
      </Canvas>
    </div>
  );
}
