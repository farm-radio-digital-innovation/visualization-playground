import React, { useState, createContext } from 'react';

function initialItems() {
  let items = [];
  for (let i = 0; i < 400; i++) {

    const posX =  Math.random() * 2000 - 1000;
    const posY =  Math.random() * 2000 - 1000;
    const posZ =  Math.random() * 2000 - 1000;

    const rotationX =  Math.random() * 200;
    const rotationY =  Math.random() * 200;
    const rotationZ =  Math.random() * 200;

    items.push({
      position: [posX, posY, posZ],
      shape: Math.floor(Math.random() * 3),
      rotation: [rotationX, rotationY, rotationZ],
    });
  }
  return items;
}

export const SceneContext = createContext({});

export function SceneContextProvider({ children }) {
  const [rotation, setRotation] = useState([0, 0, 0]);
  const [mousePosition, setMousePosition] = useState({
      left: 0,
      top: 0,
  });

  const [items, setItems] = useState(initialItems());

  const toggleSelected = (i) => {
    setItems(items.map((item, j) => ({ ...item, selected: i === j  })));
  };

  const sceneContext = {
    rotation,
    setRotation,
    mousePosition,
    setMousePosition,
    items,
    toggleSelected,
  };

  return (
    <SceneContext.Provider value={sceneContext}>
      {children}
    </SceneContext.Provider>
  );
}
